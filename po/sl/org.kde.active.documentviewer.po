# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Jure Repinc <jlp@holodeck1.com>, 2013.
# Andrej Mernik <andrejm@ubuntu.si>, 2013, 2016.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2019, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-03 00:44+0000\n"
"PO-Revision-Date: 2022-08-05 07:29+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Andrej Mernik <andrejm@ubuntu.si>\n"
"X-Generator: Lokalize 22.04.1\n"

#: package/contents/ui/Bookmarks.qml:20 package/contents/ui/OkularDrawer.qml:85
msgid "Bookmarks"
msgstr "Zaznamki"

#: package/contents/ui/CertificateViewerDialog.qml:21
msgid "Certificate Viewer"
msgstr "Pregledovalnik potrdil"

#: package/contents/ui/CertificateViewerDialog.qml:32
msgid "Issued By"
msgstr "Izdajatelj"

#: package/contents/ui/CertificateViewerDialog.qml:37
#: package/contents/ui/CertificateViewerDialog.qml:65
msgid "Common Name:"
msgstr "Splošno ime:"

#: package/contents/ui/CertificateViewerDialog.qml:43
#: package/contents/ui/CertificateViewerDialog.qml:71
msgid "EMail:"
msgstr "E-naslov:"

#: package/contents/ui/CertificateViewerDialog.qml:49
#: package/contents/ui/CertificateViewerDialog.qml:77
msgid "Organization:"
msgstr "Organizacija:"

#: package/contents/ui/CertificateViewerDialog.qml:60
msgid "Issued To"
msgstr "Izdano za"

#: package/contents/ui/CertificateViewerDialog.qml:88
msgid "Validity"
msgstr "Veljavnost"

#: package/contents/ui/CertificateViewerDialog.qml:93
msgid "Issued On:"
msgstr "Izdano:"

#: package/contents/ui/CertificateViewerDialog.qml:99
msgid "Expires On:"
msgstr "Preteče:"

#: package/contents/ui/CertificateViewerDialog.qml:110
msgid "Fingerprints"
msgstr "Prstni odtisi"

#: package/contents/ui/CertificateViewerDialog.qml:115
msgid "SHA-1 Fingerprint:"
msgstr "Prstni odtis SHA-1:"

#: package/contents/ui/CertificateViewerDialog.qml:121
msgid "SHA-256 Fingerprint:"
msgstr "Prstni odtis SHA-256:"

#: package/contents/ui/CertificateViewerDialog.qml:135
msgid "Export..."
msgstr "Izvozi ..."

#: package/contents/ui/CertificateViewerDialog.qml:141
#: package/contents/ui/SignaturePropertiesDialog.qml:148
msgid "Close"
msgstr "Zapri"

#: package/contents/ui/CertificateViewerDialog.qml:149
msgid "Certificate File (*.cer)"
msgstr "Datoteka potrdila (*.cer)"

#: package/contents/ui/CertificateViewerDialog.qml:164
#: package/contents/ui/SignaturePropertiesDialog.qml:168
msgid "Error"
msgstr "Napaka"

#: package/contents/ui/CertificateViewerDialog.qml:166
msgid "Could not export the certificate."
msgstr "Potrdila ni bilo mogoče izvoziti."

#: package/contents/ui/main.qml:23 package/contents/ui/main.qml:64
msgid "Okular"
msgstr "Okular"

#: package/contents/ui/main.qml:40
msgid "Open..."
msgstr "Odpri ..."

#: package/contents/ui/main.qml:47
msgid "About"
msgstr "O programu"

#: package/contents/ui/main.qml:104
msgid "Password Needed"
msgstr "Potrebno geslo"

#: package/contents/ui/MainView.qml:25
msgid "Remove bookmark"
msgstr "Odstrani zaznamek"

#: package/contents/ui/MainView.qml:25
msgid "Bookmark this page"
msgstr "Zaznamuj to stran"

#: package/contents/ui/MainView.qml:82
msgid "No document open"
msgstr "Ni odprtih dokumentov"

#: package/contents/ui/OkularDrawer.qml:57
msgid "Thumbnails"
msgstr "Sličice"

#: package/contents/ui/OkularDrawer.qml:71
msgid "Table of contents"
msgstr "Kazalo vsebine"

#: package/contents/ui/OkularDrawer.qml:99
msgid "Signatures"
msgstr "Podpisi"

#: package/contents/ui/SignaturePropertiesDialog.qml:30
msgid "Signature Properties"
msgstr "Lastnosti podpisa"

#: package/contents/ui/SignaturePropertiesDialog.qml:44
msgid "Validity Status"
msgstr "Stanje veljavnosti"

#: package/contents/ui/SignaturePropertiesDialog.qml:50
msgid "Signature Validity:"
msgstr "Veljavnost podpisa:"

#: package/contents/ui/SignaturePropertiesDialog.qml:56
msgid "Document Modifications:"
msgstr "Spremembe dokumenta:"

#: package/contents/ui/SignaturePropertiesDialog.qml:63
msgid "Additional Information"
msgstr "Dodatne informacije"

#: package/contents/ui/SignaturePropertiesDialog.qml:72
msgid "Signed By:"
msgstr "Podpisal:"

#: package/contents/ui/SignaturePropertiesDialog.qml:78
msgid "Signing Time:"
msgstr "Čas podpisovanja:"

#: package/contents/ui/SignaturePropertiesDialog.qml:84
msgid "Reason:"
msgstr "Razlog:"

#: package/contents/ui/SignaturePropertiesDialog.qml:91
msgid "Location:"
msgstr "Mesto:"

#: package/contents/ui/SignaturePropertiesDialog.qml:100
msgid "Document Version"
msgstr "Različica dokumenta"

#: package/contents/ui/SignaturePropertiesDialog.qml:110
msgctxt "Document Revision <current> of <total>"
msgid "Document Revision %1 of %2"
msgstr "Revizija dokumenta %1 od %2"

#: package/contents/ui/SignaturePropertiesDialog.qml:114
msgid "Save Signed Version..."
msgstr "Shrani podpisano različico ..."

#: package/contents/ui/SignaturePropertiesDialog.qml:128
msgid "View Certificate..."
msgstr "Pokaži potrdilo ..."

#: package/contents/ui/SignaturePropertiesDialog.qml:170
msgid "Could not save the signature."
msgstr "Podpisa ni bilo mogoče shraniti."

#: package/contents/ui/Signatures.qml:27
msgid "Not Available"
msgstr "Ni na voljo"

#: package/contents/ui/ThumbnailsBase.qml:43
msgid "No results found."
msgstr "Ni najdenih rezultatov."
